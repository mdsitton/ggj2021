using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AIController : MonoBehaviour
{
    public NavMeshAgent agent;
    public Animator animator;

    public FMODUnity.StudioEventEmitter snarl;

    private void OnEnable()
    {
        InvokeRepeating("PlaySnarl", 0.0f, 1.5f);
    }

    public void PlaySnarl()
    {
        snarl.Play();
    }

    void Update()
    {
        agent.destination = Camera.main.transform.position;
    }
}
