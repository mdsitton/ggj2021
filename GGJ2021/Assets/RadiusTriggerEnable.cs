using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RadiusTriggerEnable : MonoBehaviour
{
    public float radius = 2.0f;

    public GameObject toEnable;

    void Update()
    {
        // Early out if the player doesn't have the gun
        if (DiscoverableManager.itemsToSpawn.Contains(DiscoverableManager.Items.GUN)) return;

        if (Vector3.SqrMagnitude(transform.position - Camera.main.transform.position) < radius * radius)
        {
            toEnable.SetActive(true);
            Destroy(gameObject);
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.cyan;
        Gizmos.DrawSphere(transform.position, radius);
    }
}
