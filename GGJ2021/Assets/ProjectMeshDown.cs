using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshCollider), typeof(MeshFilter))]
public class ProjectMeshDown : MonoBehaviour
{
    [Range(0.02f, 0.15f)]
    public float offset = 0.05f;
    public LayerMask layerMask;

    public void ProjectMesh()
    {
        var mc = GetComponent<MeshCollider>();
        
        var verts = mc.sharedMesh.vertices;

        for (int i = 0; i < verts.Length; i++)
        {
            if (Physics.Raycast(new Ray(verts[i] + Vector3.up * 2.0f, Vector3.down), out RaycastHit hitInfo, 20.0f, layerMask, QueryTriggerInteraction.Ignore))
            {
                verts[i] = hitInfo.point + Vector3.up * offset;
            }
        }

        mc.sharedMesh.vertices = verts;
        mc.sharedMesh.RecalculateBounds();

        GetComponent<MeshFilter>().sharedMesh = mc.sharedMesh;
    }
}

#if UNITY_EDITOR

[UnityEditor.CustomEditor(typeof(ProjectMeshDown))]
public class ProjectMeshDownEditor : UnityEditor.Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        if (GUILayout.Button("Project Mesh onto Colliders"))
        {
            (target as ProjectMeshDown).ProjectMesh();
        }
    }

}

#endif