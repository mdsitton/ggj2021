using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class DiscoverableManager : MonoBehaviour
{
    public static DiscoverableManager instance 
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<DiscoverableManager>();
            }
            return _instance;
        }
    }
    private static DiscoverableManager _instance;

    public enum Items
    { 
        COIN,
        GUN,
        KEY,
        SHACKLES,
        LOCKET,
    }

    [Header("Coins or similar trinkets")]
    public GameObject[] coinPrefabs;

    public GameObject gunPrefab;
    public GameObject keyPrefab;
    public GameObject shacklesPrefab;
    public GameObject locketPrefab;

    public static List<Items> itemsToSpawn = new List<Items>();

    [Header("Jumpscare sounds")]
    public GameObject distantScream;
    public GameObject distantRoar;
    public GameObject lessDistantRoar;

    private void Start()
    {
        // Create the pseudo-random gameplay here!!

        // First pickup 1-3 coins
        itemsToSpawn.AddRange(Enumerable.Repeat(Items.COIN, Random.Range(1, 3)));

        // Then pick up a gun
        itemsToSpawn.Add(Items.KEY);

        // Then pickup 1-2 coins
        itemsToSpawn.AddRange(Enumerable.Repeat(Items.COIN, Random.Range(1, 2)));

        // Then pick up shackles
        itemsToSpawn.Add(Items.SHACKLES);

        // Then pick up a locket
        itemsToSpawn.Add(Items.LOCKET);

        // Then pick up one coin
        itemsToSpawn.Add(Items.COIN);

        // Then pick up a key
        itemsToSpawn.Add(Items.GUN);
    }

    public void SpawnItemAtPosition(Vector3 location)
    {
        if (itemsToSpawn.Count != 0)
        { 
            switch (itemsToSpawn[0])
            {
                case Items.COIN:
                    GameObject.Instantiate(coinPrefabs[Random.Range(0, coinPrefabs.Length)], location, Quaternion.identity, null);
                    break;
                case Items.GUN:
                    GameObject.Instantiate(gunPrefab, location, Quaternion.identity, null);

                    Invoke("PlayLessDistantRoar", Random.Range(3.0f, 9.0f));
                    break;
                case Items.KEY:
                    GameObject.Instantiate(keyPrefab, location, Quaternion.identity, null);
                    Invoke("PlayDistantScream", Random.Range(3.0f, 9.0f));
                    break;
                case Items.SHACKLES:
                    GameObject.Instantiate(shacklesPrefab, location, Quaternion.identity, null);
                    break;
                case Items.LOCKET:
                    GameObject.Instantiate(locketPrefab, location, Quaternion.identity, null);
                    Invoke("PlayDistantRoar", Random.Range(3.0f, 9.0f));
                    break;
                default:
                    break;
            }

            itemsToSpawn.RemoveAt(0);
        }
    }

    public void PlayDistantScream()
    {
        distantScream.SetActive(true);
    }

    public void PlayDistantRoar()
    {
        distantRoar.SetActive(true);
    }

    public void PlayLessDistantRoar()
    {
        lessDistantRoar.SetActive(true);
    }
}
