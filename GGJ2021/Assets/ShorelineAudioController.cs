using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShorelineAudioController : MonoBehaviour
{
    public Transform[] linePoints;
    public GameObject shorelineAudio;

    private void OnValidate()
    {
        linePoints = GetComponentsInChildren<Transform>();
    }

    public static Vector3 NearestPointOnFiniteLine(Vector3 start, Vector3 end, Vector3 point)
    {
        var line = (end - start);
        var len = line.magnitude;
        line.Normalize();

        var v = point - start;
        var d = Vector3.Dot(v, line);
        d = Mathf.Clamp(d, 0f, len);
        return start + line * d;
    }


    Vector3 FindClosestPointOnLineStrip(Vector3 point)
    {
        if (linePoints.Length >= 2)
        {
            Vector3 closestPoint = linePoints[0].position;
 
            for (int i = 0; i < linePoints.Length - 1; i++)
            {
                Vector3 start = linePoints[i].position;
                Vector3 end = linePoints[i + 1].position;

                Vector3 nearest = NearestPointOnFiniteLine(start, end, point);

                if (Vector3.SqrMagnitude(point - nearest) < Vector3.SqrMagnitude(point - closestPoint))
                {
                    closestPoint = nearest;
                }
            }

            return closestPoint;
        }
        else
        {
            Debug.LogWarning("Caution: Shoreline audio controller needs at least 2 points to work");
            return Vector3.zero;
        }
    }

    private void Update()
    {
        Vector3 closestPoint = FindClosestPointOnLineStrip(Camera.main.transform.position);
        shorelineAudio.transform.position = Vector3.MoveTowards(shorelineAudio.transform.position, closestPoint, 1.0f);
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;

        if (linePoints.Length >= 2)
        {
            for (int i = 0; i < linePoints.Length - 1; i++)
            {
                Gizmos.DrawLine(linePoints[i].position, linePoints[i + 1].position);
            }
        }

        Gizmos.DrawLine(Camera.main.transform.position, shorelineAudio.transform.position);
    }
}
