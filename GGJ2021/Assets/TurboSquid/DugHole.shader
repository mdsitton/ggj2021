Shader "David/DugHole"
{
    Properties
    {
        _NoiseTex("Noise texture", 2D) = "white" {}
        _GrassTex("Grass (RGB)", 2D) = "white" {}
        _GrassColor("Grass Color", Color) = (0,1,0,1)
        _Height("Height", Range(0.0001,5)) = 1.0
    }

        SubShader
        {
            Tags { "RenderType" = "Opaque" "Queue" = "Geometry"}
            ZWrite Off ZTest LEqual
            Offset -2, -2

            CGPROGRAM

            #pragma surface surf Lambert vertex:vert
            #pragma target 3.0


            //Input
            sampler2D _NoiseTex;
            sampler2D _GrassTex;
            float4 _GrassColor;
            float _Height;


            struct Input
            {
                //What Unity can give you
                float2 uv_NoiseTex;

                //What you have to calculate yourself
                float3 tangentViewDir;
            };


            void vert(inout appdata_full i, out Input o)
            {
                UNITY_INITIALIZE_OUTPUT(Input, o);

                //Transform the view direction from world space to tangent space			
                float3 worldVertexPos = mul(unity_ObjectToWorld, i.vertex).xyz;
                float3 worldViewDir = worldVertexPos - _WorldSpaceCameraPos;

                //To convert from world space to tangent space we need the following
                //https://docs.unity3d.com/Manual/SL-VertexFragmentShaderExamples.html
                float3 worldNormal = UnityObjectToWorldNormal(i.normal);
                float3 worldTangent = UnityObjectToWorldDir(i.tangent.xyz);
                float3 worldBitangent = cross(worldNormal, worldTangent) * i.tangent.w * unity_WorldTransformParams.w;

                //Use dot products instead of building the matrix
                o.tangentViewDir = float3(
                    dot(worldViewDir, worldTangent),
                    dot(worldViewDir, worldNormal),
                    dot(worldViewDir, worldBitangent)
                    );
            }

            //Get the height from a uv position
            float getHeight(float2 texturePos)
            {
                //Multiply with 0.2 to make the landscape flatter
                float4 colorNoise = tex2Dlod(_NoiseTex, float4(texturePos, 0, 0));

                float height = (1 - colorNoise.r) * -1 * _Height;

                return height;
            }


            //Combine stone and grass depending on grayscale color
            float4 getBlendTexture(float2 texturePos, float height)
            {
                float4 colorGrass = tex2Dlod(_GrassTex, float4(texturePos, 0, 0)) * _GrassColor;

                return colorGrass;
            }


            //Get the texture position by interpolation between the position where we hit terrain and the position before
            float2 getWeightedTexPos(float3 rayPos, float3 rayDir, float stepDistance)
            {
                //Move one step back to the position before we hit terrain
                float3 oldPos = rayPos - stepDistance * rayDir;

                float oldHeight = getHeight(oldPos.xz);

                //Always positive
                float oldDistToTerrain = abs(oldHeight - oldPos.y);

                float currentHeight = getHeight(rayPos.xz);

                //Always negative
                float currentDistToTerrain = rayPos.y - currentHeight;

                float weight = currentDistToTerrain / (currentDistToTerrain - oldDistToTerrain);

                //Calculate a weighted texture coordinate
                //If height is -2 and oldHeight is 2, then weightedTex is 0.5, which is good because we should use 
                //the exact middle between the coordinates
                float2 weightedTexPos = oldPos.xz * weight + rayPos.xz * (1 - weight);

                return weightedTexPos;
            }


            void surf(Input IN, inout SurfaceOutput o)
            {
                //Where is the ray starting? y is up and we always start at the surface
                float3 rayPos = float3(IN.uv_NoiseTex.x, 0, IN.uv_NoiseTex.y);

                //What's the direction of the ray?
                float3 rayDir = normalize(IN.tangentViewDir);

                //Find where the ray is intersecting with the terrain with a raymarch algorithm
                int STEPS = 300;
                float stepDistance = 0.001;

                //The default color used if the ray doesnt hit anything
                float4 finalColor = 1;

                for (int i = 0; i < STEPS; i++)
                {
                    //Get the current height at this uv coordinate
                    float height = getHeight(rayPos.xz);

                    //If the ray is below the surface
                    if (rayPos.y < height)
                    {
                        //Get the texture position by interpolation between the position where we hit terrain and the position before
                        float2 weightedTex = getWeightedTexPos(rayPos, rayDir, stepDistance);

                        float height = getHeight(weightedTex);

                        finalColor = getBlendTexture(weightedTex, height);
                        o.Alpha = 1 - tex2Dlod(_NoiseTex, float4(rayPos.xz, 0, 0)).r;

                        //We have hit the terrain so we dont need to loop anymore	
                        break;
                    }

                    //Move along the ray
                    rayPos += stepDistance * rayDir;
                }

                //Output
                o.Albedo = finalColor.rgb * smoothstep(o.Alpha, 0.2, 0.25);

                if (o.Alpha < 0.2) discard;
            }
            ENDCG
        }
            FallBack "Diffuse"
}