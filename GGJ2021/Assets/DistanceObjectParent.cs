using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DistanceObjectParent : MonoBehaviour
{
    List<DistanceDetect> childObjects;

    public float radius;

    private void Start()
    {
        childObjects = new List<DistanceDetect>(transform.GetComponentsInChildren<DistanceDetect>());
    }

    private void LateUpdate()
    {
        if (MetalDetectorHead.instance != null)
        {
            if (Vector3.SqrMagnitude(MetalDetectorHead.instance.transform.position - transform.position) < radius * radius)
            {
                foreach (DistanceDetect child in childObjects)
                {
                    child.gameObject.SetActive(true);
                }
            }
            else if (Vector3.SqrMagnitude(MetalDetectorHead.instance.transform.position - transform.position) > ((radius + 3.0f) * (radius + 3.0f)))
            {

                foreach (DistanceDetect child in childObjects)
                {
                    child.gameObject.SetActive(false);
                }
            }
        }
    }

    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(transform.position, radius);
    }
}