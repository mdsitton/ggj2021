using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DiscoverableItem : MonoBehaviour
{
    public GameObject holeObject;
    public GameObject hintHole;
    public LayerMask initialPlacementCastMask;
    
    void Start()
    {
        if (Physics.Raycast(new Ray(transform.position + Vector3.up, Vector3.down), out RaycastHit hitInfo, 
            10.0f, initialPlacementCastMask, QueryTriggerInteraction.Ignore))
        {
            transform.position = hitInfo.point;
            transform.up = hitInfo.normal;
            transform.Rotate(Vector3.up, Random.Range(0.0f, 360.0f), Space.Self);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Shovel")
        {
            ShowHole();
            SpawnItem();
        }

        Destroy(this);
    }

    public void ShowHole()
    {
        hintHole.SetActive(false);
        holeObject.SetActive(true);
    }

    public void SpawnItem()
    {
        Vector3 projectedCameraPos = Vector3.ProjectOnPlane(Camera.main.transform.position, transform.up);
        Vector3 projectedThisPos = Vector3.ProjectOnPlane(transform.position, transform.up);

        // spawn the item 60cm on the opposite side of the hole (not in the hole, not under the player)
        Vector3 idealSpawnPosition = transform.position + Vector3.Normalize(projectedThisPos - projectedCameraPos) * 0.6f;

        DiscoverableManager.instance.SpawnItemAtPosition(idealSpawnPosition);
    }
}

#if UNITY_EDITOR
[UnityEditor.CustomEditor(typeof(DiscoverableItem))]
public class DiscoverableItemEditor : UnityEditor.Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        if (GUILayout.Button("Open Discoverable"))
        {
            (target as DiscoverableItem).ShowHole();
            (target as DiscoverableItem).SpawnItem();
        }
    }
}

#endif