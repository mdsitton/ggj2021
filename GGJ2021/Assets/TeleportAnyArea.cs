using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;

public class TeleportAnyArea : TeleportMarkerBase
{

    //-------------------------------------------------
    public void Awake()
    {
    }


    //-------------------------------------------------
    public void Start()
    {
    }


    //-------------------------------------------------
    public override bool ShouldActivate(Vector3 playerPosition)
    {
        return true;
    }


    //-------------------------------------------------
    public override bool ShouldMovePlayer()
    {
        return true;
    }


    //-------------------------------------------------
    public override void Highlight(bool highlight)
    {
    }


    //-------------------------------------------------
    public override void SetAlpha(float tintAlpha, float alphaPercent)
    {
    }


    //-------------------------------------------------
    public override void UpdateVisuals()
    {
    }
}
