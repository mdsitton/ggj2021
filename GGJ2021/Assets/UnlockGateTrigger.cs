using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnlockGateTrigger : MonoBehaviour
{
    public Transform[] gates;
    public GameObject metallicOpenSound;

    [HideInInspector]
    public bool gateIsOpened = false;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player" && gateIsOpened == false)   
        {
            // If the key doesn't exist (has been spawned already)
            if (!DiscoverableManager.itemsToSpawn.Contains(DiscoverableManager.Items.KEY))
            {
                StartCoroutine(OpenGates());
            }
        }
    }

    public void OpenGateNotCoroutine()
    {
        gateIsOpened = false;
        StartCoroutine(OpenGates());
    }

    // Ease-in-ease-out
    float Gain(float x, float k)
    {
        float a = 0.5f * Mathf.Pow(2.0f * ((x < 0.5f) ? x : 1.0f - x), k);
        return (x < 0.5f) ? a : 1.0f - a;
    }

    IEnumerator OpenGates()
    {
        metallicOpenSound.SetActive(true);
        gateIsOpened = true;

        // Take about 3 seconds to open
        for (float t = 0.0f; t < 1.0f; t += Time.deltaTime * 0.333f)
        {
            gates[0].localEulerAngles = new Vector3(gates[0].localEulerAngles.x, Gain(t, 1.5f) * 80.0f, gates[0].localEulerAngles.z);
            gates[1].localEulerAngles = new Vector3(gates[1].localEulerAngles.x, Gain(t, 1.5f) * -80.0f, gates[1].localEulerAngles.z);

            yield return new WaitForEndOfFrame();    
        } 

        yield return null;
    }
}

#if UNITY_EDITOR

[UnityEditor.CustomEditor(typeof(UnlockGateTrigger))]
public class UnlockGateTriggerEditor : UnityEditor.Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        if (GUILayout.Button("Test the Gate"))
        {
            (target as UnlockGateTrigger).OpenGateNotCoroutine();
        }
    }
}

#endif