using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Valve.VR;

public class GunController : MonoBehaviour
{
    public ParticleSystem muzzleFlash;
    public GameObject bulletHolePrefab;

    public FMODUnity.StudioEventEmitter pistolEvent;

    public SteamVR_Action_Single fire = SteamVR_Input.GetAction<SteamVR_Action_Single>("Fire");

    public void Shoot()
    {
        pistolEvent.Play();

        muzzleFlash.Play(true);
        if (Physics.Raycast(new Ray(muzzleFlash.transform.position, transform.forward), out RaycastHit hitInfo, 1000.0f))
        {
            var obj = Instantiate(bulletHolePrefab, hitInfo.point, Quaternion.identity);
            obj.transform.forward = hitInfo.normal;
            if (hitInfo.collider.gameObject.tag == "Monster")
            {
                Destroy(hitInfo.collider.gameObject);
            }
        }
    }

    bool hasFired = false;

    void Update()
    {
        if (!hasFired && fire.axis > 0.5)
        {
            Shoot();
            hasFired = true;
        }
        else if (hasFired && fire.axis < 0.1f)
        {
            hasFired = false;
        }
    }
}

#if UNITY_EDITOR
[UnityEditor.CustomEditor(typeof(GunController))]
public class GunControllerEditor : UnityEditor.Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        if (GUILayout.Button("Shoot"))
        {
            (target as GunController).Shoot();
        }
    }
}

#endif