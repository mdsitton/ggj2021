using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MetalDetectorHead : MonoBehaviour
{
    public static MetalDetectorHead instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<MetalDetectorHead>();
            }

            return _instance;
        }
    }
    private static MetalDetectorHead _instance;
    public FMODUnity.StudioEventEmitter metalDetectorSpatialized;

    public float sensitivity = 1.0f;

    [Header("Debug variables - Readonly please")]
    public float nearestDistance = 0.0f;
    public GameObject foundObject;

    private bool isTurnedOn = false;

    // Testing, remove this later probably
    private void Start()
    {
        PowerOn();
    }

    public void PowerOn()
    {
        isTurnedOn = true;
        metalDetectorSpatialized.Play();    
    }

    public void PowerOff()
    {
        isTurnedOn = false;
        metalDetectorSpatialized.Stop();
    }

    void Update()
    {
        if (isTurnedOn)
        {
            float nearestObjectDist = float.MaxValue;

            var objects = FindObjectsOfType<DistanceDetect>();

            foreach (var obj in objects)
            {
                float calculatedDistance = obj.CalculateDistance(transform.position);
                if (calculatedDistance < nearestObjectDist)
                {
                    nearestObjectDist = calculatedDistance;

                    foundObject = obj.gameObject;
                    nearestDistance = calculatedDistance;
                }
                
            }

            metalDetectorSpatialized.SetParameter("MetalLevel", Mathf.Clamp01(1.0f - (nearestObjectDist / sensitivity)));
        }
    }

    private void OnDrawGizmos()
    {
        if (foundObject != null)
        {
            Gizmos.color = Color.yellow;
            Gizmos.DrawWireSphere(foundObject.transform.position, 0.25f);
            Gizmos.DrawLine(transform.position, foundObject.transform.position);
        }
    }
}
