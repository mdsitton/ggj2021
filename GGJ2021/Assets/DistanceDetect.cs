using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DistanceDetect : MonoBehaviour
{
    public enum DetectType
    {
        SPHERE = 0,
        BOX = 1,
        LINE = 2,
    }

    public DetectType type;

    public float sphereRadius = 1.0f;

    public float capsuleHeight = 1.0f;
    public float capsuleRadius = 0.25f;
    public Vector3 boxBounds = new Vector3(1.0f, 1.0f, 1.0f);

    public bool isStatic = true;
    Matrix4x4 trsInternalInverted;

    private void Start()
    {
        trsInternalInverted = Matrix4x4.TRS(transform.position, transform.rotation, Vector3.one).inverse;
    }

    private void Update()
    {
        if (!isStatic)
        {
            trsInternalInverted = Matrix4x4.TRS(transform.position, transform.rotation, Vector3.one).inverse;
        }
    }

    private Vector3 V3Abs(Vector3 x)
    {
        return new Vector3(Mathf.Abs(x.x), Mathf.Abs(x.y), Mathf.Abs(x.z));
    }

    private float sdVerticalCapsule(Vector3 p, float h, float r)
    {
        p.y -= Mathf.Clamp(p.y, 0.0f, h);
        return Vector3.Magnitude(p) - r;
    }

    private float sdBox(Vector3 p, Vector3 b)
    {
        Vector3 q = V3Abs(p) - b;
        return Vector3.Magnitude(Vector3.Max(q, Vector3.zero)) + Mathf.Min(Mathf.Max(q.x, Mathf.Max(q.y, q.z)), 0.0f);
    }

    private float sdSphere(Vector3 p, float s)
    {
        return Vector3.Magnitude(p) - s;
    }

    public float CalculateDistance(Vector3 worldPosition)
    {
        Vector3 relativePosition = trsInternalInverted.MultiplyPoint(worldPosition);

        switch (type)
        {
            case DetectType.SPHERE:
                return sdSphere(relativePosition, sphereRadius);
            case DetectType.BOX:
                return sdBox(relativePosition, boxBounds * 0.5f);
            case DetectType.LINE:
                return sdVerticalCapsule(relativePosition, capsuleHeight, capsuleRadius);
            default:
                break;
        }

        return float.MaxValue;
    }

    void OnDrawGizmos()
    {
        Gizmos.matrix = Matrix4x4.TRS(transform.position, transform.rotation, Vector3.one);

        Gizmos.color = new Color(1.0f, 1.0f, 1.0f, 0.1f);

        switch (type)
        {
            case DetectType.SPHERE:
                Gizmos.DrawWireSphere(Vector3.zero, sphereRadius);
                break;

            case DetectType.BOX:
                Gizmos.DrawWireCube(Vector3.zero, boxBounds);
                break;

            case DetectType.LINE:
                Gizmos.DrawLine(Vector3.zero, Vector3.up * capsuleHeight);

                Gizmos.DrawWireSphere(Vector3.zero, capsuleRadius);
                Gizmos.DrawWireSphere(Vector3.up * capsuleHeight, capsuleRadius);
                break;

            default:
                break;
        }
    }
}
